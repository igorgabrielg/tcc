import pandas as pd
import logging
import os


def load_data():
    logging.info(f'Carregamento das bases de dados iniciado.')

    vaccination_path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'base', 'programa_nacional_de_imunizacoes', 'part2.csv')
    df = pd.read_csv(vaccination_path, sep=';')

    return df


def data_processing(df):
    logging.info(f'Analise dos dados iniciado.')

    # Seleciona as colunas de interesse do DataFrame
    df = df[['paciente_id', 'vacina_descricao_dose', 'vacina_dataAplicacao', 'paciente_idade', 'estabelecimento_uf']]

    df['vacina_descricao_dose'] = df['vacina_descricao_dose'].map({'1ª Dose': '1',
                                 '2ª Dose': '2',
                                 '2º Reforço': '2',
                                 '3º Reforço': '3',
                                 'Dose': '1',
                                 'Dose Adicional': '2',
                                 'Reforço': '3'},
                                na_action=None)

    df = df.rename(columns={
        'vacina_descricao_dose': 'num_doses',
        'vacina_dataAplicacao': 'data_aplicacao',
        'paciente_idade': 'idade',
        'estabelecimento_uf': 'uf'
    })

    return df

    # Troca o nome da dose por quantidade de doses aplicadas


def save_csv(df, csv_name):
    path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'result', 'programa_nacional_de_imunizacoes', csv_name)

    df.to_csv(path, index=False)

    logging.info(f'Dados atualizado!')
    logging.info(f'Arquivo salvo em: {path}')


def run():
    df = load_data()
    df = data_processing(df)
    save_csv(df, "part2.csv")

    print("Finalizado!")


if __name__ == '__main__':
    run()