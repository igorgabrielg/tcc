import numpy as np
import pandas as pd
import logging
import matplotlib.dates as mpl_dates
from matplotlib import pyplot as plot, pyplot as plt
import seaborn as sns
import os


def load_data():
    logging.info(f'Carregamento das bases de dados iniciado.')
    print(f'Carregamento das bases de dados iniciado.')

    ocupation2021 = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'base', 'registro_de_ocupacao_hospitalar_covid', 'leito_ocupacao_2021.csv')
    ocupation2022 = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'base', 'registro_de_ocupacao_hospitalar_covid', 'leito_ocupacao_2022.csv')
    df2021 = pd.read_csv(ocupation2021, sep=',')
    df2022 = pd.read_csv(ocupation2022, sep=',')

    return df2021, df2022


def data_processing(df):
    logging.info(f'Analise dos dados iniciado.')

    # Seleciona as colunas de interesse do DataFrame
    df = df[[
        'estado',
        'dataNotificacao',
        'ocupacaoSuspeitoCli',
        'ocupacaoSuspeitoUti',
        'ocupacaoConfirmadoCli',
        'ocupacaoConfirmadoUti',
        'ocupacaoCovidCli',
        'ocupacaoCovidUti',
        'ocupacaoHospitalarCli',
        'ocupacaoHospitalarUti',
        'saidaSuspeitaObitos',
        'saidaSuspeitaAltas',
        'saidaConfirmadaObitos',
        'saidaConfirmadaAltas']]

    # Renomeia as colunas
    df = df.rename(columns={
        'dataNotificacao': 'data_notificacao',
        'ocupacaoSuspeitoCli': 'ocupacao_suspeito_cli',
        'ocupacaoSuspeitoUti': 'ocupacao_suspeito_uti',
        'ocupacaoConfirmadoCli': 'ocupacao_confirmado_cli',
        'ocupacaoConfirmadoUti': 'ocupacao_confirmado_uti',
        'ocupacaoCovidCli': 'ocupacao_covid_cli',
        'ocupacaoCovidUti': 'ocupacao_covid_uti',
        'ocupacaoHospitalarCli': 'ocupacao_hospitalar_cli',
        'ocupacaoHospitalarUti': 'ocupacao_hospitalar_uti',
        'saidaSuspeitaObitos': 'saida_suspeita_obitos',
        'saidaSuspeitaAltas': 'saida_suspeita_altas',
        'saidaConfirmadaObitos': 'saida_confirmada_obitos',
        'saidaConfirmadaAltas': 'saida_confirmada_altas',
    })

    # Converte data e hora em data
    df['data_notificacao'] = pd.to_datetime(df['data_notificacao']).dt.date
    # Filtra pelo estado de Goias
    df = df[df['estado'] == 'Goiás']
    # Remove coluna estado
    df = df.drop(columns=['estado'])
    # Ordena por data notificação
    df = df.groupby(['data_notificacao'], as_index=False).sum().reset_index()

    df['data_notificacao'] = pd.to_datetime(df['data_notificacao'], errors='coerce')
    df = df.groupby(df.data_notificacao.dt.month)[['ocupacao_hospitalar_uti', 'ocupacao_hospitalar_cli', 'saida_confirmada_altas', 'saida_confirmada_obitos']].sum().reset_index()


    df = df.rename(columns={
        'data_notificacao': 'Data notificação',
        'ocupacao_hospitalar_uti': 'Ocupacao UTI',
        'ocupacao_hospitalar_cli': 'Ocupacao CLI',
        'saida_confirmada_altas': 'Saida Confir/Altas',
        'saida_confirmada_obitos': 'Saida Confir/Obitos'
    })

    sns.set(rc={'figure.figsize':(12,7)})
    print("total_doses_aplicadas", "total_ocupacao_uti")
    sns.heatmap(df.corr(), annot=True)
    plt.show()
    print("--")

    print("teste")
    print(df)

    print(df)

    return df


def save_csv(df, csv_name):
    path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'result', 'registro_de_ocupacao_hospitalar_covid', csv_name)

    df.to_csv(path, index=False)

    logging.info(f'Dados atualizado!')
    logging.info(f'Arquivo salvo em: {path}')


def plotTest(df2021, df2022):
    print("Iniciando plotagem!")

    df2021['data_notificacao'] = pd.Series(np.array(['Janeiro 2021', 'Fevereiro 2021', 'Março 2021', 'Abril 2021', 'Maio 2021', 'Junho 2021', 'Julho 2021', "agosto 2021", "setembro 2021", "outubro 2021", "novembro 2021", "dezembro 2021"]))
    df2022['data_notificacao'] = pd.Series(np.array(['Janeiro 2022', 'Fevereiro 2022', 'Março 2022', 'Abril 2022', 'Maio 2022', 'Junho 2022', 'Julho 2022', "agosto 2022", "setembro 2022", "outubro 2022", "novembro 2022", "dezembro 2022"]))

    df = pd.concat([df2021, df2022], ignore_index=True)

    print(df)

    df.drop(df.index[(df['data_notificacao'] == 'Janeiro 2021') | (df['data_notificacao'] == 'Fevereiro 2021') | (df['data_notificacao'] == 'Março 2021') | (df['data_notificacao'] == 'Abril 2021') | (df['data_notificacao'] == 'Maio 2021')], inplace=True)

    # df['data_notificacao'] = pd.Series(np.array(['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', "agosto", "setembro", "outubro", "novembro", "dezembro"]))

    line = sns.lineplot(df['data_notificacao'], df['ocupacao_hospitalar_uti'])
    line.set_xticklabels(df['data_notificacao'], rotation=90)
    plt.xticks(df['data_notificacao'])

    plt.show()

    save_csv(df, "leito_ocupacao_2021_2022.csv")


def run():
    df2021, df2022 = load_data()
    df2021 = data_processing(df2021)
    df2022 = data_processing(df2022)

    # # save_csv(df, "leito_ocupacao_2022.csv")
    # plotTest(df2021, df2022)

    print("Finalizado!")


if __name__ == '__main__':
    run()