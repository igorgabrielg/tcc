import pandas as pd
import logging
import os


def load_data():
    logging.info(f'Carregamento das bases de dados iniciado.')

    vaccination_path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'base', 'programa_nacional_de_imunizacoes', 'meus', 'part2.csv')
    df = pd.read_csv(vaccination_path, sep=';')

    return df


def data_processing(df):
    logging.info(f'Analise dos dados iniciado.')

    # Seleciona as colunas de interesse do DataFrame
    df = df[['paciente_id', 'vacina_descricao_dose', 'vacina_dataAplicacao']]

    df['vacina_descricao_dose'] = df['vacina_descricao_dose'].map({'1ª Dose': '1',
                                 '2ª Dose': '1',
                                 '2º Reforço': '1',
                                 '3º Reforço': '1',
                                 'Dose': '1',
                                 'Dose Adicional': '1',
                                 'Reforço': '1'},
                                na_action=None)

    df = df.rename(columns={
        'vacina_descricao_dose': 'num_doses',
        'vacina_dataAplicacao': 'data_aplicacao',
        'paciente_id': 'id'
    })

    print(df)

    df = df.groupby(['data_aplicacao', 'id'], as_index=False).sum().reset_index()

    df = df.drop(columns=['id'])

    df = df.groupby(['data_aplicacao'], as_index=False).sum().reset_index()

    # print(df[['Name', 'Qualification']])

    return df

    # Troca o nome da dose por quantidade de doses aplicadas


def save_csv(df, csv_name):
    path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'result', 'programa_nacional_de_imunizacoes', csv_name)

    df.to_csv(path, index=False)

    logging.info(f'Dados atualizado!')
    logging.info(f'Arquivo salvo em: {path}')


def run():
    df = load_data()
    df = data_processing(df)
    save_csv(df, "part2.csv")

    print("Finalizado!")


if __name__ == '__main__':
    run()