import logging
import os

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt


def load_data():
    logging.info(f'Carregamento das bases de dados iniciado.')

    vaccination_path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'base', 'programa_nacional_de_imunizacoes', 'parte2.csv')
    df = pd.read_csv(vaccination_path, sep=';')

    return df


def data_processing(df):

    # Obtem apenas a data da vacina aplicada
    df = df[['vacina_dataAplicacao']]
    # Conta todas as vacinas aplicadas do dia em doses_aplicada
    df["doses_aplicadas"] = df.assign(date_col = lambda x: x.index).groupby(['vacina_dataAplicacao']).transform('count')
    # Remove as linhas com data duplicada
    df = df.drop_duplicates(subset='vacina_dataAplicacao', keep="first")
    # Transforma vacina_dataAplicacao no tipo data
    df["vacina_dataAplicacao"] = pd.to_datetime(df["vacina_dataAplicacao"])
    # Ordena por vacina_dataAplicacao
    df = df.sort_values(by="vacina_dataAplicacao")

    df2021 = data_processing2021(df.loc[(df['vacina_dataAplicacao'] > '2021-6-1') & (df['vacina_dataAplicacao'] <= '2021-12-31')])
    df2022 = data_processing2022(df.loc[(df['vacina_dataAplicacao'] > '2022-1-1') & (df['vacina_dataAplicacao'] <= '2022-7-31')])

    # Agrupa as doses por mês
    df_graphic = df.groupby(df.vacina_dataAplicacao.dt.month)['doses_aplicadas'].sum().reset_index()

    df20212022 = pd.concat([df2021, df2022], ignore_index=True)

    line = sns.lineplot(df20212022['vacina_dataAplicacao'], df20212022['doses_aplicadas'])
    line.set_xticklabels(df20212022['vacina_dataAplicacao'], rotation=90)
    plt.xticks(df20212022['vacina_dataAplicacao'])
    plt.show()

    print(df20212022)

    save_csv(df20212022, "doses_mensal.csv")

    return df


def data_processing2021(df):
    df = df.groupby(df.vacina_dataAplicacao.dt.month)['doses_aplicadas'].sum().reset_index()

    df['vacina_dataAplicacao'] = pd.Series(np.array(
        [f'Junho 2021', f'Julho 2021',
         f"agosto 2021", f"setembro 2021", f"outubro 2021", f"novembro 2021", f"dezembro 2021"]))

    return df


def data_processing2022(df):
    df = df.groupby(df.vacina_dataAplicacao.dt.month)['doses_aplicadas'].sum().reset_index()

    df['vacina_dataAplicacao'] = pd.Series(np.array(
        [f'Janeiro 2022', f'Fevereiro 2022',
         f"Março 2022", f"Abril 2022", f"Maio 2022", f"Junho 2022", f"Julho 2022"]))

    return df


def save_csv(df, csv_name):
    path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'result', 'programa_nacional_de_imunizacoes', csv_name)

    print(df)
    df.to_csv(path, index=False)

    logging.info(f'Dados atualizado!')
    logging.info(f'Arquivo salvo em: {path}')


def run():
    df = load_data()
    df = data_processing(df)
    save_csv(df, "doses_diaria.csv")

    print("Finalizado!")


if __name__ == '__main__':
    run()