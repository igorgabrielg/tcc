import logging
import os

import numpy as np
import pandas as pd
import seaborn as sns
from matplotlib import pyplot as plt
from matplotlib.pyplot import axis


def load_data():
    logging.info(f'Carregamento das bases de dados iniciado.')

    ocupacao_path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'result',
                                    'registro_de_ocupacao_hospitalar_covid', 'leito_ocupacao_2021_2022.csv')

    vacinacao_path = os.path.join(os.path.abspath('../../..'), 'data', 'csv', 'result',
                                    'programa_nacional_de_imunizacoes', 'doses_mensal.csv')


    dfOcupacao = pd.read_csv(ocupacao_path)

    dfVacinacao = pd.read_csv(vacinacao_path)

    return dfOcupacao, dfVacinacao


def save_analysis_chart(dfOcupacao, dfVacinacao):

    dfOcupacao.columns = ['data', 'ocupacao_hospitalar_uti']
    dfVacinacao.columns = ['data', 'doses_aplicadas']

    df = pd.merge(dfOcupacao, dfVacinacao, how='inner', on='data')

    df.plot()
    #
    # plt.title('Título')  # adicionando o título
    # # plt(df['data'], rotation=90)
    #
    #
    # # plt.set_xticklabels(df['data'], rotation=90)
    #

    figure, axis = plt.subplots(2)
 
    # For Sine Function
    axis[0].plot(df['data'], df['ocupacao_hospitalar_uti'])
    axis[0].set_title("Ocupação hospitalar UTI")
    axis[0].tick_params(axis='x', rotation=45)

    axis[1].plot(df['data'], df['doses_aplicadas'])
    axis[1].set_title("Doses mensal aplicadas")
    axis[1].tick_params(axis='x', rotation=45)

    plt.gcf().subplots_adjust(left=0.15, bottom=0.2, top=0.92, right=0.93, wspace=0.4,
                              hspace=1.2)


    df['total_doses_aplicadas'] = df['doses_aplicadas'].cumsum()
    df['total_ocupacao_uti'] = df['ocupacao_hospitalar_uti'].cumsum()

    figure2, axis2 = plt.subplots(1)
    axis2.tick_params(axis='x', rotation=45)
    axis2.set_title("Total de doses aplicadas")

    plt.plot(df['data'], df['total_doses_aplicadas'])
    plt.plot(df['data'], df['total_ocupacao_uti'])
    print(df['total_doses_aplicadas'])

    plt.gcf().subplots_adjust(left=0.15, bottom=0.2, top=0.92, right=0.93, wspace=0.4,
                    hspace=1.2)

    plt.legend(['Total de doses aplicadas', 'Ocupação total'], title="Legenda")

    plt.show()

    plt.savefig('C:\\Users\\igor\\OneDrive\\Documentos\\projetos\\ifg\\IA\\tcc\\visualisation\\ranking_members.png', dpi=300)


    # save_csv(df20212022, "doses_mensal.csv")

    return df


def run():
    dfOcupacao, dfVacinacao = load_data()
    df = save_analysis_chart(dfOcupacao, dfVacinacao)
    # correlacao(df)


    print("Finalizado!")


def correlacao(df):
    correlation = df.corr()

    plot = sns.heatmap(correlation, annot=True, fmt=".1f", linewidths=.6)
    print(plot)
    print(df)


if __name__ == '__main__':
    run()