import numpy as np
import matplotlib.pyplot as plt
import matplotlib
import pandas as pd
import seaborn as sns

import io

sns.set_context('talk')
plt.style.use('seaborn-whitegrid')


def load_timeseries(name, database_directory=''):
    path = f'{database_directory}/.csv'

    print(f"A base de dados utilizada é: {path}")
    csv = pd.read_csv(path)

    df = pd.read_csv(io.StringIO(csv),
                     index_col=['Estado', 'Cidade', 'Lat', 'Long'])
    df['type'] = name.lower()
    df.columns.name = 'date'

    df = (df.set_index('type', append=True)
          .reset_index(['Lat', 'Long'], drop=True)
          .stack()
          .reset_index()
          .set_index('date')
          )
    df.index = pd.to_datetime(df.index)
    df.columns = ['estado', 'cidade', 'type', 'cases']

    # Move HK to country level
    df.loc[df.state == 'Goiás', 'estado'] = 'Goiás'
    df.loc[df.state == 'Goiás', 'cidade'] = np.nan

    # Aggregate large countries split by states
    df = pd.concat([df,
                    (df.loc[~df.state.isna()]
                     .groupby(['estado', 'date', 'type'])
                     .sum()
                     .rename(index=lambda x: x + ' (total)', level=0)
                     .reset_index(level=['cidade', 'type']))
                    ])
    return df