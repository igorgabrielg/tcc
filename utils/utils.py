import glob
import os


def save_csv(df, csv_name):
    """
    Salva o csv apartir de um DataFrame com o nome.

    :param df:
    :param path:
    :param csv_name:
    """
    path = os.path.join(os.path.abspath('..'), 'data', 'csv', 'base', csv_name)

    df.to_csv(path, index=True)
    print(path)