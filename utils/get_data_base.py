import numpy as np
import pandas as pd
import os

import requests
import io


def get_data_base(url='https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/time_series_covid19_confirmed_global.csv'):

    csv = requests.get(url).text
    print(csv)
    df = pd.read_csv(io.StringIO(csv), index_col=['Country/Region', 'Province/State', 'Lat', 'Long'])

    return df


def save_csv(df, csv_name):
    path = os.path.join(os.path.abspath('..'), 'data', 'csv', 'base', csv_name)

    df.to_csv(path, index=True)


def run():
    df = get_data_base()
    print(df)
    save_csv(df, "time_series_covid19_confirmed_global.csv")


if __name__ == '__main__':
    run()