Pasta destinada para armazenar os arquivos de imagem de visualização.  

    ── development
        ├── nome_da_analise
            ├── data_analysis_type
                ├── visualisation.png
                ...
    ── concluded
        ├── nome_da_analise
            ├── data_analysis_type
                ├── visualisation.png
                ...