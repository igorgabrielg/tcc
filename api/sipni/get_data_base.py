import numpy as np
import pandas as pd
import json
import logging
import os

import requests
import io


def request_api():
    logging.info(f'Inicio da atualização da fonte de dados da Campanha Nacional de Vacinação contra Covid-19.')

    url = "https://imunizacao-es.saude.gov.br/_search"
    username = "imunizacao_public"
    password = "qlto5t&7r_@+#Tlstigi"

    response = requests.get(url, auth=(username, password))

    df = pd.DataFrame.from_dict(response.json(), orient='columns')
    print(df)

    return df


def save_csv(df, csv_name):
    path = os.path.join(os.path.abspath('../..'), 'data', 'csv', 'base', 'programa_nacional_de_imunizacoes', csv_name)

    df.to_csv(path, index=True)

    logging.info(f'Dados atualizado!')
    logging.info(f'Arquivo salvo em: {path}')


def run():
    df = request_api()
    save_csv(df, "programa_nacional_de_imunizacoes.csv")


if __name__ == '__main__':
    run()