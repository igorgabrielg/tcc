Pasta destinada a atualizações através de uma Interface de Programa de Aplicativos(API). Com ela, é possível ter um 
serviço de consulta direta aos dados de diversas bases de dados sem precisar navegar pelo site ou utilizar robôs para a
obtenção das informações de forma automática. Os dados disponíveis são os mesmos apresentados em tela, com a 
flexibilidade característica das APIs.