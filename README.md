# TCC

## Objetivos

O objetivo do trabalho é identificar possíveis correlações em bases de dados sobre o corona vírus no Brasil 
conduzidos pelo Ministério da Saúde e IBGE, e inferir se há relação de causa e efeito entre elas, por fim, revelar os 
resultados em uma plataforma com mapas e dashboards de fácil entendimento

## Bases de dados

- [Dados Abertos](https://)
- [e-SUS Atenção Primária ](https://)
- [FAPESP COVID-19 DataSharing/BR](https://)
- [Instituto Brasileiro de Geografia e Estatística](https://)
- [Open Data SUS](https://)
- [Portal da Transparência Registro Civil](https://)

## Artigo científico

[TCC](https://www.overleaf.com/read/pkxfnzxgkjjr)


## Funcionalisades

[Análise de crescimento (Em desenvolvimento)](https://gitlab.com/igorgabrielg/tcc/-/tree/main/data_analysis/growth_analysis)

## Instalação do ambiente virtual

```
pip install -r requirements.txt
```